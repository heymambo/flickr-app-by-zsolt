import React from 'react';
import styles from './AppFooter.module.css';

function AppFooter() {
    return(
        <div className={styles.container}>
            <p>Flickr App</p>
        </div>
    )
}

export default AppFooter;