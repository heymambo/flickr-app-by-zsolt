import React, { useEffect } from 'react';
import styles from './ImagesView.module.css';
import ImageBox from './ImageBox';
import ShowMoreButton from './ShowMoreButton';


function ImagesView(props) {
    
    useEffect(() => {
            if(props.showMoreButtonClicked) {
                window.addEventListener('scroll', handleScroll);
                return () => window.removeEventListener('scroll', handleScroll);
            }
            function handleScroll() {
                if (window.innerHeight + document.documentElement.scrollTop === document.documentElement.scrollHeight) {
                   props.showMoreHandler();
                }
            }
      }, [props]);


    let filteredPhotos = props.photos.photo.filter((photo) => {
        return photo.title.toLowerCase().includes(props.searchquery.toLowerCase())
    })

    let slicedPhotos = filteredPhotos.slice(0, props.numberOfItemsToShow);
   
    let showMore = (props.thereAreMoreItems && (slicedPhotos.length >= props.numberOfItemsToShow)) ? true : false;


    return(
        <div className={styles.container}>

            {slicedPhotos.map((photo) => 
                <ImageBox imagedata={photo} key={photo.id} season={props.season}/>
            )}

            <ShowMoreButton showMore={showMore} season={props.season} showMoreHandler={props.showMoreHandler} showMoreButtonClickedHandler={props.showMoreButtonClickedHandler}/> 

        </div>
    )
        
}

export default ImagesView;