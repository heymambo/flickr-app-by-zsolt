import React from 'react';
import styles from './ErrorView.module.css';

function ErrorView() {
    return(
        <div className={styles.container}>
            <h2>Something went wrong. Please try again!</h2>
        </div>
    )
}

export default ErrorView;