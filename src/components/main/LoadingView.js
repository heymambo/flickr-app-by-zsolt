import React from 'react';
import styles from './LoadingView.module.css';

function LoadingView() {
    return(
        <div className={styles.container}>
            <h2>Loading...</h2>
        </div>
    )
}

export default LoadingView;