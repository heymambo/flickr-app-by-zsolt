import React from 'react';
import classNames from "classnames";
import styles from './ShowMoreButton.module.css';


function ShowMoreButton(props) {

    function bundleOnClickEvents(event) {
        props.showMoreHandler();
        props.showMoreButtonClickedHandler();
    }

    return(
        <div className={styles.container}>
            {props.showMore ?
            <button className={styles.button} onClick={bundleOnClickEvents} data-season={props.season}>Show more</button>
            : <button className= {classNames(styles.button, styles.disabled)}>No more photos</button>
            }
        </div>
    )
}


export default ShowMoreButton;