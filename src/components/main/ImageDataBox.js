import React from 'react';
import styles from './ImageDataBox.module.css';
import ImageTitle from './ImageTitle';
import ImageAuthor from './ImageAuthor';
import ImageDescription from './ImageDescription';
import ImageTags from './ImageTags';


function ImageDataBox(props) {

    return(       
        
        <div className={styles.container}>

            <ImageTitle title={props.imagedata.title} photoid={props.imagedata.id} ownerid={props.imagedata.owner} season={props.season}/>
            <ImageAuthor author={props.imagedata.ownername} ownerid={props.imagedata.owner}/>
            <ImageDescription description={props.imagedata.description._content} />
            <ImageTags tags={props.imagedata.tags} season={props.season} />

        </div>
    )

}


export default ImageDataBox;