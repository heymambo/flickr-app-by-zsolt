import React from 'react';
import styles from './ImageAuthor.module.css';

function ImageAuthor(props) {

    return (

        <div className={styles.container}>
            <h3>by <a href={`https://www.flickr.com/people/${props.ownerid}/`} target="_blank" rel="noreferrer noopener">{props.author}</a></h3>
        </div>

    )
}

export default ImageAuthor;