import React from 'react';
import styles from './ImageDescription.module.css';
// import TextTruncate from 'react-text-truncate';
import Truncate from 'react-truncate-html';


function ImageDescription(props) {

    
    return (
     
        <div className={styles.container}>
            <Truncate
                lines={3}
                dangerouslySetInnerHTML={{__html: props.description}}
            />
        </div>

    )
}

export default ImageDescription;