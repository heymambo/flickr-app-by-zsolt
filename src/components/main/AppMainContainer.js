import React from 'react';
import LoadingView from './LoadingView';
import ImagesView from './ImagesView';
import ErrorView from './ErrorView';


const AppMainContainer = ({ loading, flickr, searchquery, numberOfItemsToShow, thereAreMoreItems, showMoreHandler, showMoreButtonClickedHandler, showMoreButtonClicked, season}) => {

    if (loading) {
      return <LoadingView />;
    } else if (flickr) {
      return <ImagesView {...flickr} searchquery={searchquery} numberOfItemsToShow={numberOfItemsToShow} thereAreMoreItems={thereAreMoreItems} showMoreHandler={showMoreHandler} showMoreButtonClickedHandler={showMoreButtonClickedHandler} showMoreButtonClicked={showMoreButtonClicked} season={season}/>;
    } else {
      return <ErrorView/>;
    }
  };

export default AppMainContainer;