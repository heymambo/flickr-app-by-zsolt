import React from 'react';
import styles from './ImageBox.module.css';
import ImageDataBox from './ImageDataBox';


function ImageBox(props) {

    return(
        <div className={styles.container}>
                
            { props.imagedata.url_z ? <img src={props.imagedata.url_z} alt=""/> : <div className={styles.noimage}>no image</div> }
              
            <ImageDataBox imagedata={props.imagedata} season={props.season}/>

        </div>
    )
}


export default ImageBox;


// function ImageBox(props) {

//     return(
//         <div className={styles.container}>
//                 <img 
//                     src={props.imagedata.url_z} 
//                     alt=""
//                 />
//                 <ImageDataBox imagedata={props.imagedata}/>

//         </div>
//     )
// }