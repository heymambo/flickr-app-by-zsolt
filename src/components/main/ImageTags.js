import React from 'react';
import styles from './ImageTags.module.css';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSortDown } from '@fortawesome/free-solid-svg-icons';

class ImageTags extends React.Component {
    

    state = {divheight: 0};

    componentDidMount() {
        const height = this.divElement.clientHeight;
        this.setState({ divHeight: height });
    }

    render(){
   

    let tagArray = this.props.tags.split(' ');
    let tagList = tagArray.map(function(tag, index){

        if (tag) {
            return (
                    <span className={styles.tags} key={index}>
                        <a
                            href={ `https://www.flickr.com/photos/tags/${tag}`} 
                            className={styles.taglink}
                            target="_blank"
                            rel="noreferrer noopener"
                        >
                        {tag}
                        </a>
                    </span>
            )
        }
        else {
            return (
                <span key={index} className={styles.tags}>
                    {null}
                </span>
            )
        }
        
    })

    let ClassNames = classNames(
        styles.container, {
        [styles.long]: this.state.divHeight > 25
        }
    )
    
        return (
            <div className={ClassNames} ref={ (divElement) => this.divElement = divElement} data-season={this.props.season}>
                {tagList} 
                { this.state.divHeight > 25 && <FontAwesomeIcon icon={faSortDown} className={styles.longIcon}/> }
            </div>
        )
    
    }
}

export default ImageTags;