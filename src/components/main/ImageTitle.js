import React from 'react';
import styles from './ImageTitle.module.css';

function ImageTitle(props) {

    return (
        <div className={styles.container}>
            <h2 title={props.title} data-season={props.season}>
                {props.title ? <a href={`https://www.flickr.com/photos/${props.ownerid}/${props.photoid}/`} target="_blank" rel="noreferrer noopener">{props.title}</a> : "Untitled"}
            </h2>
        </div>
    )
    
}

export default ImageTitle;