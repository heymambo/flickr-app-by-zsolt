import React from 'react';
import Brand from './Brand';
import Menu from './Menu';
import SearchForm from './SearchForm';
import styles from './AppHeader.module.css';


function AppHeader(props){
        return ( 
        <div className={styles.container}>
            <div className={styles.wrapper}>
                <div className={styles.brandColumn}>
                    <Brand/>
                </div>
                <div className={styles.menuAndSearchColumn}>
                    <Menu gallerySwitchHandler={props.gallerySwitchHandler} season={props.season}/>
                    <SearchForm flickrFilter={props.flickrFilter} submitHandler={props.submitHandler} />
                </div>
            </div>
        </div>
    )
}

export default AppHeader;