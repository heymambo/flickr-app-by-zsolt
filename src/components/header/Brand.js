import React from 'react';
import styles from './Brand.module.css';

function Brand(props){

    return(
        <div className={styles.logo}>
                <h1>Flickr App</h1>
                <h2><span>by </span>Zsolt</h2> 
        </div>
    )
    
}

export default Brand;