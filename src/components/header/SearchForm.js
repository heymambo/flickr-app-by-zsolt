import React from 'react';
import styles from './SearchForm.module.css';

function SearchForm(props) {

    return(
        <div className={styles.container}>
            <form onSubmit={props.submitHandler}>
                <fieldset>
                    <input type="text" placeholder="Search on page by title" onChange={props.flickrFilter}/>
                </fieldset>
            </form>
        </div>
    )

}

export default SearchForm;