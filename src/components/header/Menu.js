import React from 'react';
import styles from './Menu.module.css';

function Menu(props) {

    const menuItems = ['spring', 'summer', 'autumn', 'winter' ];

    return(
        <div className={styles.container}>
            <ul>
                {menuItems.map((menuItem) => 
                    <li key={menuItem.toString()}>
                        <button className={props.season === menuItem ? styles.active : styles.notActive} onClick={props.gallerySwitchHandler} value={menuItem}>{menuItem}</button>
                    </li>
                )}
            </ul>
        </div>
    )
}

export default Menu;




