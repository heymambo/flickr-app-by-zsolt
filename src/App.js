import React, { Component } from 'react';
import axios from 'axios';
import './App.css';
import styles from './app.module.css';
import AppHeader from './components/header/AppHeader';
import AppMainContainer from './components/main/AppMainContainer';
import AppFooter from './components/footer/AppFooter';
import Scroll from 'react-scroll';
import {getSeason} from './utility/cszsUtilities';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretSquareUp } from '@fortawesome/free-solid-svg-icons';


const FLICKR_GROUP_ID_SPRING = "1057257@N20";
const FLICKR_GROUP_ID_SUMMER = "1206243@N23";
const FLICKR_GROUP_ID_AUTUMN = "95697387@N00";
const FLICKR_GROUP_ID_WINTER = "16378484@N00";
const scroll = Scroll.animateScroll;

let now = new Date();
let currentMonth = now.getMonth();
let getSeasonResult = getSeason(currentMonth, FLICKR_GROUP_ID_SPRING, FLICKR_GROUP_ID_SUMMER, FLICKR_GROUP_ID_AUTUMN, FLICKR_GROUP_ID_WINTER);
let currentSeason = getSeasonResult.season;
let currentGallery = getSeasonResult.photoStream;

class App extends Component {

  state = { loading: true, searchquery: "", numberOfItemsToShow: 8, gallery: currentGallery, thereAreMoreItems: true, showMoreButtonClicked: false, season: currentSeason };
  

  componentDidMount() {
    axios.get(`https://8gohe3030k.execute-api.us-east-1.amazonaws.com/production/main?group_id=${encodeURIComponent(this.state.gallery)}`)
      .then(response => {
          const flickr = response.data;
          this.setState({ loading: false, flickr })
        }
      )
      .catch(error => {
          this.setState({ loading: false, error })
        }
      )
  }

  componentDidUpdate(prevProps, prevState) {

    if (this.state.gallery !== prevState.gallery) {
    axios.get(`https://8gohe3030k.execute-api.us-east-1.amazonaws.com/production/main?group_id=${encodeURIComponent(this.state.gallery)}`)
      .then(response => {
          const flickr = response.data;
          this.setState({ loading: false, flickr })
        }
      )
      .catch(error => {
          this.setState({ loading: false, error })
        }
      )
    } 
  }

  gallerySwitchHandler = (event) => {
    if(this.state.season!==event.target.value) {
      switch (event.target.value) {
        case 'spring': this.setState({
          season: "spring",
          gallery: FLICKR_GROUP_ID_SPRING,
          numberOfItemsToShow: 8, 
          showMoreButtonClicked: false, 
          thereAreMoreItems: true,
          loading: true
        });
          break;

        case 'summer': this.setState({
          season: "summer",
          gallery: FLICKR_GROUP_ID_SUMMER, 
          numberOfItemsToShow: 8, 
          showMoreButtonClicked: false, 
          thereAreMoreItems: true,
          loading: true
        });
        break;

        case 'autumn': this.setState({
          season: "autumn",
          gallery: FLICKR_GROUP_ID_AUTUMN, 
          numberOfItemsToShow: 8, 
          showMoreButtonClicked: false, 
          thereAreMoreItems: true,
          loading: true
        });
          break;

        case 'winter': this.setState({
          season: "winter",
          gallery: FLICKR_GROUP_ID_WINTER, 
          numberOfItemsToShow: 8, 
          showMoreButtonClicked: false, 
          thereAreMoreItems: true,
          loading: true
        });
          break;
      
        default: console.log("default");
          break;
      }
    }
  }

  flickrFilter = (event) => {
    this.setState({searchquery: event.target.value}); 
  }

  submitHandler = (event) => {
      event.preventDefault();
  }

  showMoreHandler = (event) => {
    this.setState({
      numberOfItemsToShow: this.state.numberOfItemsToShow >= this.state.flickr.photos.photo.length ?
      this.state.numberOfItemsToShow : this.state.numberOfItemsToShow + 8,

      thereAreMoreItems: this.state.numberOfItemsToShow <= this.state.flickr.photos.photo.length ?
      true : false,
    })
  }

  showMoreButtonClickedHandler = (event) => {
    this.setState({
      showMoreButtonClicked: true
    })
  }

  scrollToTop() {
    scroll.scrollToTop({ 
        duration: 1500, 
        smooth: 'easeInOutQuart'
    });
  }

  render() {
    return (
      <div className={styles.container}>
        <AppHeader flickrFilter={this.flickrFilter} submitHandler={this.submitHandler} gallerySwitchHandler={this.gallerySwitchHandler} season={this.state.season}/>
        <AppMainContainer {...this.state} showMoreHandler={this.showMoreHandler} showMoreButtonClickedHandler={this.showMoreButtonClickedHandler}/>
        <AppFooter/>
        <FontAwesomeIcon className={styles.toTheTopButton} icon={faCaretSquareUp} onClick={this.scrollToTop}/>
      </div>
    );
  }
}

export default App;