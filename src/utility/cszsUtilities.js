/**
 * 
 * @param {number} month 
 * @param {srting} springPhotoStream 
 * @param {string} summerPhotoStream 
 * @param {string} autumnPhotoStream 
 * @param {string} winterPhotoStream 
 */


export function getSeason(month, springPhotoStream, summerPhotoStream, autumnPhotoStream, winterPhotoStream) {
    if (2 <= month && month <= 4) {
      return {
        season: 'spring',
        photoStream: springPhotoStream
      }
    }
  
    else if (5 <= month && month <= 7) {
      return {
        season: 'summer',
        photoStream: summerPhotoStream
      }
    }
  
    else if (8 <= month && month <= 10) {
      return {
        season: 'autumn',
        photoStream: autumnPhotoStream
      }
    }
    
    else {
      return {
        season: 'winter',
        photoStream: winterPhotoStream
      }
    } 
}